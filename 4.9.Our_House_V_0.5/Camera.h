#pragma once

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>

class Camera {
public:
	bool isOrtho;
	glm::vec3 prp, vrp, vup;
	glm::mat4 ViewMatrix;

	GLfloat aspectRatio = 1, zoomFactor = 1;
	GLfloat nearDist = 10, farDist = 10000, nearH, nearW;
	glm::mat4 ProjectionMatrix;

public:
	Camera() {}
	Camera(glm::vec3 const &eye, glm::vec3 const &center, glm::vec3 const &up) {
		replaceCameraParams(eye, center, up);
	}

	void setupOrtho(int width, int height) {
		nearW = width;
		nearH = height;
		aspectRatio = (float)width / height;
		ProjectionMatrix = glm::ortho(-nearW / 2, nearW / 2, -nearH / 2, nearH / 2, nearDist, farDist);

		isOrtho = true;
	}

	void setupPerspect(GLfloat asp, GLfloat zf) {
		GLfloat const fov = 15;
		nearH = 2 * nearDist*glm::tan(glm::radians(fov / 2));
		nearW = nearH * (aspectRatio = asp);
		ProjectionMatrix = glm::frustum(-nearW / 2, nearW / 2, -nearH / 2, nearH / 2, nearDist, farDist);

		isOrtho = false;
	}

	glm::mat4 const& _calcPerspective() const {
		if(isOrtho) return ProjectionMatrix;
		else return glm::scale(glm::mat4(), glm::vec3(zoomFactor, zoomFactor, 1)) * ProjectionMatrix;
	}

	glm::mat4 const &replaceCameraParams(glm::vec3 const &eye, glm::vec3 const &center, glm::vec3 const &up) {
		prp = eye;
		vrp = center;
		ViewMatrix = glm::lookAt(prp, vrp, up);
		vup = glm::vec3(glm::row(ViewMatrix, 1));

		return ViewMatrix;
	}

	/*
	void updateAsp(GLfloat asp) {
		aspectRatio = asp;
		nearW = nearH * aspectRatio;

		GLfloat n;
		if(isOrtho) n = 1;
		else n = nearDist;
		ProjectionMatrix[0][0] = 2 * n / nearW;
	}
	*/

	glm::mat4 const& _getViewMatrix() const {
		return ViewMatrix;
	}

	/* (b)-i. */
	void translate(glm::vec3 disp) {
		// (actual disp) = [u v n] * (disp)
		disp = glm::transpose(glm::mat3(ViewMatrix)) * disp;
		prp += disp;
		vrp += disp;
		ViewMatrix = glm::lookAt(prp, vrp, vup);
	}

	/* (b)-ii. */
	void rotate(GLfloat alpha, glm::vec3 axis) {
		// (actual axis) = [u v n] * (axis)
		axis = glm::transpose(glm::mat3(ViewMatrix)) * axis;

		glm::mat4 m;
		m = glm::translate(glm::mat4(), prp);
		m = glm::rotate(m, alpha, axis);
		m = glm::translate(m, -prp);

		vrp = glm::vec3(m * glm::vec4(vrp, 1));
		vup = glm::vec3(m * glm::vec4(vup, 0));
		ViewMatrix = glm::lookAt(prp, vrp, vup);
	}

	/* (b)-iii. */
	void zoom(GLfloat zf) {
		zoomFactor = zf;
	}

};

void defineViewVolumeObject(GLuint *vao, GLuint *vbo) {
	static GLfloat plane[4][2] = {
		{.5, .5}, {-.5, .5}, {-.5, -.5}, {.5, -.5}
	};
	static GLfloat lines[4][2][3] = {
		{{0, 0, 0}, {.5, .5, -1}},
		{{0, 0, 0}, {-.5, .5, -1}},
		{{0, 0, 0}, {-.5, -.5, -1}},
		{{0, 0, 0}, {.5, -.5, -1}},
	};

	glGenVertexArrays(2, vao);
	glGenBuffers(2, vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBindVertexArray(vao[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(plane), plane, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, static_cast<GLvoid *>(0));
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBindVertexArray(vao[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lines), lines, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, static_cast<GLvoid *>(0));
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void updateViewVolumObject(GLuint *vao, GLuint *vbo, Camera const &camera) {
	// 필요없을거같다

	static GLfloat planes[2][4][3];
	static GLfloat boundLines[4][2][3];

	glm::vec4 nUL(-camera.nearW / 2, camera.nearH / 2, -camera.nearDist, 1);
	glm::vec4 nUR(camera.nearW / 2, camera.nearH / 2, -camera.nearDist, 1);
	glm::vec4 nBL(-camera.nearW / 2, -camera.nearH / 2, -camera.nearDist, 1);
	glm::vec4 nBR(camera.nearW / 2, -camera.nearH / 2, -camera.nearDist, 1);

	glm::mat4 ToFar;
	ToFar = glm::scale(ToFar, glm::vec3(1, 1, camera.farDist / camera.nearDist));

	glm::vec4 fUL = ToFar * nUL;
	glm::vec4 fUR = ToFar * nUR;
	glm::vec4 fBL = ToFar * nBL;
	glm::vec4 fBR = ToFar * nBR;
	
	std::vector<glm::vec4> list;
	list = {nUL, nUR, nBL, nBR, fUL, fUR, fBL, fBR};
	for(int i = 0; i < list.size(); ++i) {
		((GLfloat *)planes)[i * 3] = list[i].x;
		((GLfloat *)planes)[i * 3 + 1] = list[i].y;
		((GLfloat *)planes)[i * 3 + 2] = list[i].z;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(planes), planes);

	list = { nUL, fUL, nUR, fUR, nBL, fBL, nBR, fBR };
	for(int i = 0; i < 8; ++i) {
		((GLfloat *)boundLines)[i * 3] = list[i].x;
		((GLfloat *)boundLines)[i * 3 + 1] = list[i].y;
		((GLfloat *)boundLines)[i * 3 + 2] = list[i].z;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(boundLines), boundLines);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void drawViewVolumeObject(GLuint *vao, GLuint *vbo, Camera const &target) {
	float const PURPLE[] = { 250 / 255., 230 / 255., 250 / 255. };
	float const YELLOW[] = { 1., 1., 0. };

	auto const &vm = target.ViewMatrix;

	glm::mat4 targetViewInv(
		vm[0].x, vm[1].x, vm[2].x, 0,
		vm[0].y, vm[1].y, vm[2].y, 0,
		vm[0].z, vm[1].z, vm[2].z, 0,
		target.prp.x, target.prp.y, target.prp.z, 1
	);

	// near, far planes
	glBindVertexArray(vao[0]);
	glUniform3fv(loc_primitive_color, 1, PURPLE);

	int nW = target.nearW / target.zoomFactor;
	int nH = target.nearH / target.zoomFactor;

	ModelViewMatrix = ViewMatrix * targetViewInv;
	ModelViewMatrix = glm::translate(ModelViewMatrix, glm::vec3(0, 0, -target.nearDist));
	ModelViewMatrix = glm::scale(ModelViewMatrix, glm::vec3(nW, nH, 1));
	ModelViewProjectionMatrix = ProjectionMatrix * ModelViewMatrix;
	glUniformMatrix4fv(loc_ModelViewProjectionMatrix, 1, GL_FALSE, &ModelViewProjectionMatrix[0][0]);
	glDrawArrays(GL_LINE_LOOP, 0, 4);

	ModelViewMatrix = ViewMatrix * targetViewInv;
	ModelViewMatrix = glm::scale(ModelViewMatrix, glm::vec3(target.farDist / target.nearDist));
	ModelViewMatrix = glm::translate(ModelViewMatrix, glm::vec3(0, 0, -target.nearDist));
	ModelViewMatrix = glm::scale(ModelViewMatrix, glm::vec3(nW, nH, 1));
	ModelViewProjectionMatrix = ProjectionMatrix * ModelViewMatrix;
	glUniformMatrix4fv(loc_ModelViewProjectionMatrix, 1, GL_FALSE, &ModelViewProjectionMatrix[0][0]);
	glDrawArrays(GL_LINE_LOOP, 0, 4);

	// lines remaining
	glBindVertexArray(vao[1]);
	glUniform3fv(loc_primitive_color, 1, YELLOW);

	float f = target.farDist;
	float W = nW* (f/target.nearDist);
	float H = nH * (f/target.nearDist);

	ModelViewMatrix = ViewMatrix * targetViewInv;
	ModelViewMatrix = glm::scale(ModelViewMatrix, glm::vec3(W, H, f));
	ModelViewProjectionMatrix = ProjectionMatrix * ModelViewMatrix;
	glUniformMatrix4fv(loc_ModelViewProjectionMatrix, 1, GL_FALSE, &ModelViewProjectionMatrix[0][0]);
	glDrawArrays(GL_LINES, 0, 2 * 4);

	glBindVertexArray(0);
}