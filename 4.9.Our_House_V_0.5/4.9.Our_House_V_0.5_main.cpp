#include <stdio.h>
#include <stdlib.h>
#include <tuple>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include "Shaders/LoadShaders.h"
GLuint h_ShaderProgram; // handle to shader program
GLint loc_ModelViewProjectionMatrix, loc_primitive_color; // indices of uniform variables

// include glm/*.hpp only if necessary
//#include <glm/glm.hpp> 
#include <glm/gtc/matrix_transform.hpp> //translate, rotate, scale, lookAt, perspective, etc.
glm::mat4 ModelViewProjectionMatrix;
glm::mat4 ModelViewMatrix, ViewMatrix, ProjectionMatrix;

#define TO_RADIAN 0.01745329252f  
#define TO_DEGREE 57.295779513f

#include "./Object_Def.h"
#include "./Path.h"
#include "Object_Definitions.h"
#include "./Camera.h"

auto constexpr NUM_CAMS = 10;

Path path;
GLuint pathVAO, pathVBO;
GLuint volVAO[2], volVBO[2];
Camera cameras[NUM_CAMS];

enum struct DisplayMode {
	Main, ObjectView, Tiger, Static1, Static2, Static3
};

struct ViewPort {
	float w, h, x, y;
} viewports[NUM_CAMS];

struct CallbackContext {
	DisplayMode dMode;
	ObjectId objId;
	int winWidth, winHeight;
	bool isStatic;
	bool isLButtonDown, isRButtonDown;
	bool isCtrlDown;
	int prevX, prevY;
	float zoomFactor;

	int timestamp;

	bool roamingTiger, isTigerCam;
	int roamBegin;
} context;

void drawObject() {
	ProjectionMatrix = ::cameras[0].ProjectionMatrix;
	ViewMatrix = ::cameras[0].ViewMatrix;
	if((int)context.objId > 7) {
		ObjectDef2::StaticObject::getStaticObject((ObjectDef2::ObjectId)context.objId).draw(0);
	}
	else {
		draw_static_object(static_objects + (int)context.objId, 0);
	}
}

void drawScene(bool isMatrixInherit = false) {
	if(context.dMode == DisplayMode::ObjectView) {
		drawObject();
		return;
	}

	if(!context.roamingTiger) draw_animated_tiger();
	else {
		// tiger is roaming
		int elapse = context.timestamp - context.roamBegin;
		float const speed = 1.5;

		auto posData = path.calcPosition(elapse * speed);
		auto position = glm::vec3(std::get<0>(posData), 0);

		auto dir = glm::normalize(std::get<1>(posData));

		glm::mat4 Mat = glm::translate(glm::mat4(), position);
		Mat *= glm::mat4(
			glm::vec4(dir, 0, 0),
			glm::vec4(-dir.y, dir.x, 0, 0),
			glm::vec4(0, 0, 1, 0),
			glm::vec4(0, 0, 0, 1)
		);

		if(context.dMode == DisplayMode::Tiger && isMatrixInherit) {
			glm::vec4 prp, vrp, vup;

			::cameras[8] = ::cameras[7];

			prp = glm::vec4(cameras[7].prp, 1);
			vrp = glm::vec4(cameras[7].vrp, 1);
			vup = glm::vec4(cameras[7].vup, 0);

			prp = Mat * prp;
			vrp = Mat * vrp;
			vup = Mat * vup;

			ViewMatrix = ::cameras[8].replaceCameraParams(glm::vec3(prp), glm::vec3(vrp), glm::vec3(vup));
			ProjectionMatrix = ::cameras[8]._calcPerspective();
		}

		drawTiger(Mat);

		ModelViewProjectionMatrix = ProjectionMatrix * ViewMatrix;
		glUniformMatrix4fv(loc_ModelViewProjectionMatrix, 1, GL_FALSE, &ModelViewProjectionMatrix[0][0]);
		glUniform3f(loc_primitive_color, 1.f, 0.f, 0.f);
		glBindVertexArray(pathVAO);
		glPointSize(5.0f);
		glDrawArrays(GL_POINTS, 0, path.path.size());
		glPointSize(1.0f);
		glBindVertexArray(0);
	}

    draw_static_object(&(static_objects[OBJ_BUILDING]), 0);

	draw_static_object(&(static_objects[OBJ_TABLE]), 0);
	draw_static_object(&(static_objects[OBJ_TABLE]), 1);

	draw_static_object(&(static_objects[OBJ_LIGHT]), 0);
	draw_static_object(&(static_objects[OBJ_LIGHT]), 1);
	draw_static_object(&(static_objects[OBJ_LIGHT]), 2);
	draw_static_object(&(static_objects[OBJ_LIGHT]), 3);
	draw_static_object(&(static_objects[OBJ_LIGHT]), 4);

	draw_static_object(&(static_objects[OBJ_TEAPOT]), 0);
	draw_static_object(&(static_objects[OBJ_NEW_CHAIR]), 0);

 	draw_static_object(&(static_objects[OBJ_FRAME]), 0);
	draw_static_object(&(static_objects[OBJ_NEW_PICTURE]), 0);
	draw_static_object(&(static_objects[OBJ_COW]), 0);

	ObjectDef2::StaticObject::getStaticObject(ObjectDef2::ObjectId::BIKE).draw(1);
	ObjectDef2::StaticObject::getStaticObject(ObjectDef2::ObjectId::BUS).draw(1);
	ObjectDef2::StaticObject::getStaticObject(ObjectDef2::ObjectId::GODZILLA).draw(1);
	ObjectDef2::StaticObject::getStaticObject(ObjectDef2::ObjectId::IRONMAN).draw(1);
	ObjectDef2::StaticObject::getStaticObject(ObjectDef2::ObjectId::TANK).draw(1);

}

void display(void) {

	auto const vps = viewports;
	auto const cams = cameras;
	glScissor(0, 0, context.winWidth, context.winHeight);

	glClearColor(1, 1, 1, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glScissor(vps[0].x, vps[0].y, vps[0].w, vps[0].h);
	glViewport(vps[0].x, vps[0].y, vps[0].w, vps[0].h);
	glClearColor(0.12f, 0.18f, 0.12f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	int main = 0;
	if(context.dMode == DisplayMode::Main) {
		main = 0;
		ViewMatrix = cams[0]._getViewMatrix();
		ProjectionMatrix = cams[0]._calcPerspective();
	}
	else if(context.dMode == DisplayMode::Static1) {
		main = 4;
		ViewMatrix = cams[4]._getViewMatrix();
		ProjectionMatrix = cams[4]._calcPerspective();
	}
	else if(context.dMode == DisplayMode::Static2) {
		main = 5;
		ViewMatrix = cams[5]._getViewMatrix();
		ProjectionMatrix = cams[5]._calcPerspective();
	}
	else if(context.dMode == DisplayMode::Static3) {
		main = 6;
		ViewMatrix = cams[6]._getViewMatrix();
		ProjectionMatrix = cams[6]._calcPerspective();
	}
	else if(context.dMode == DisplayMode::Tiger) {
		main = 8;
		// View, Projection not here
	}

	drawScene(true);

	for(int c = 1; c < 4; ++c) {
		glScissor(vps[c].x, vps[c].y, vps[c].w, vps[c].h);
		glViewport(vps[c].x, vps[c].y, vps[c].w, vps[c].h);
		glClearColor(0.08f, 0.18f, 0.12f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		ViewMatrix = cams[c]._getViewMatrix();
		ProjectionMatrix = cams[c]._calcPerspective();

		drawScene();
		drawViewVolumeObject(volVAO, volVBO, cams[main]);
	}

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
	Camera *pcam = nullptr;
	if(context.dMode == DisplayMode::Main) {
		pcam = &::cameras[0];
	}
	else if(context.dMode == DisplayMode::Tiger) {
		pcam = &::cameras[7];
	}

	static int flag_cull_face = 0, polygon_fill_on = 0, depth_test_on = 0;

	bool actionDone = true;

	switch (key) {
	case 27: // ESC key
		glutLeaveMainLoop(); // Incur destuction callback for cleanups.
		break;
	case '1':
		context.dMode = DisplayMode::Main;
		context.isStatic = false;
		break;
	case '2':
		context.dMode = DisplayMode::ObjectView;
		context.isStatic = true;
		break;
	case '3':
		if(context.roamingTiger) {
			context.isStatic = false;
			context.dMode = DisplayMode::Tiger;
		}
		break;
	case '4':
		context.dMode = DisplayMode::Static1;
		context.isStatic = true;
		break;
	case '5':
		context.dMode = DisplayMode::Static2;
		context.isStatic = true;
		break;
	case '6':
		context.dMode = DisplayMode::Static3;
		context.isStatic = true;
		break;
	case 'n':
		context.objId = static_cast<ObjectId>((static_cast<int>(context.objId) + 1) % 16);
		break;
	case 'd':
		if(!pcam) break;
		if(context.isStatic) break;
		pcam->translate(glm::vec3(1, 0, 0));
		ViewMatrix = pcam->_getViewMatrix();
		break;
	case 'w':
		if(!pcam) break;
		if(context.isStatic) break;
		pcam->translate(glm::vec3(0, 1, 0));
		ViewMatrix = pcam->_getViewMatrix();
		break;
	case 'a':
		if(!pcam) break;
		if(context.isStatic) break;
		pcam->translate(glm::vec3(-1, 0, 0));
		ViewMatrix = pcam->_getViewMatrix();
		break;
	case 's':
		if(!pcam) break;
		if(context.isStatic) break;
		pcam->translate(glm::vec3(0, -1, 0));
		ViewMatrix = pcam->_getViewMatrix();
		break;
	case 'z':
		if(!pcam) break;
		if(context.isStatic) break;
		pcam->translate(glm::vec3(0, 0, 5));
		ViewMatrix = pcam->_getViewMatrix();
		break;
	case 'x':
		if(!pcam) break;
		if(context.isStatic) break;
		pcam->translate(glm::vec3(0, 0, -5));
		ViewMatrix = pcam->_getViewMatrix();
		break;
	case 'g':
		context.roamingTiger = true;
		context.roamBegin = context.timestamp;
		break;
	case 'h':
		context.roamingTiger = false;
		break;
	case '0':
		if(!pcam) break;
		context.zoomFactor = 1;
		pcam->zoom(context.zoomFactor = 1);
		ProjectionMatrix = pcam->_calcPerspective();
		break;
	case 'c':
		flag_cull_face = (flag_cull_face + 1) % 3;
		switch (flag_cull_face) {
		case 0:
			glDisable(GL_CULL_FACE);
			fprintf(stdout, "^^^ No faces are culled.\n");
			break;
		case 1: // cull back faces;
			glCullFace(GL_BACK);
			glEnable(GL_CULL_FACE);
			fprintf(stdout, "^^^ Back faces are culled.\n");
			break;
		case 2: // cull front faces;
			glCullFace(GL_FRONT);
			glEnable(GL_CULL_FACE);
			fprintf(stdout, "^^^ Front faces are culled.\n");
			break;
		}
		break;
	case 'f':
		polygon_fill_on = 1 - polygon_fill_on;
		if (polygon_fill_on) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			fprintf(stdout, "^^^ Polygon filling enabled.\n");
		}
		else {
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			fprintf(stdout, "^^^ Line drawing enabled.\n");
		}
		break;
	case 'e':
		depth_test_on = 1 - depth_test_on;
		if (depth_test_on) {
			glEnable(GL_DEPTH_TEST);
			fprintf(stdout, "^^^ Depth test enabled.\n");
		}
		else {
			glDisable(GL_DEPTH_TEST);
			fprintf(stdout, "^^^ Depth test disabled.\n");
		}
		break;
	default:
		actionDone = false;
		break;
	}

	if(actionDone) {
		glutPostRedisplay();
	}
}

void mouseCb(int button, int state, int x, int y) {
	if(button == GLUT_LEFT_BUTTON) {
		if(state == GLUT_DOWN) {
			context.prevX = x; context.prevY = y;
			context.isLButtonDown = true;
			glutPostRedisplay();
		}
		else if(state == GLUT_UP) {
			context.isLButtonDown = false;
			glutPostRedisplay();
		}
	}
	else if(button == GLUT_RIGHT_BUTTON) {
		if(state == GLUT_DOWN) {
			context.prevY = y;
			context.isRButtonDown = true;
			glutPostRedisplay();
		}
		else if(state == GLUT_UP) {
			context.isRButtonDown = false;
			glutPostRedisplay();
		}
	}

	if(glutGetModifiers() & GLUT_ACTIVE_CTRL) {
		context.isCtrlDown = true;
	}
	else {
		context.isCtrlDown = false;
	}
}

#define CAM_ROT_SENSITIVITY 0.000872 // pi/360px
#define CAM_ZOOM_SENSITIVITY 0.01
void motionCb(int x, int y) {
	if(context.isStatic) return;
	Camera *pcam = nullptr;
	if(context.dMode == DisplayMode::Main) {
		pcam = &::cameras[0];
	}
	else if(context.dMode == DisplayMode::Tiger) {
		pcam = &::cameras[7];
	}

	if(context.isLButtonDown) {
		int delX = (x - context.prevX), delY = -(y - context.prevY);
		context.prevX = x; context.prevY = y;

		if(context.isCtrlDown) {
			context.zoomFactor += delY * CAM_ZOOM_SENSITIVITY;
			if(context.zoomFactor > 5) context.zoomFactor = 5;
			else if(context.zoomFactor < 0.1) context.zoomFactor = 0.1;

			pcam->zoom(context.zoomFactor);
			ProjectionMatrix = pcam->_calcPerspective();
			glutPostRedisplay();
		}
		else {
			pcam->rotate(glm::sqrt(delX*delX + delY * delY)*CAM_ROT_SENSITIVITY, glm::vec3(-delY, delX, 0));
			ViewMatrix = pcam->_getViewMatrix();
			glutPostRedisplay();
		}
	}
	else if(context.isRButtonDown) {
		int delY = -(y - context.prevY);
		context.prevY = y;

		pcam->rotate(delY*CAM_ROT_SENSITIVITY, glm::vec3(0, 0, 1));
		ViewMatrix = pcam->_getViewMatrix();
		glutPostRedisplay();
	}
}

void reshape(int width, int height) {

	auto const vps = viewports;
	auto const cams = cameras;
	float _h, _w;

	int pad = 5;
	glm::vec3 metric(240, 170, 50);
	auto rh = metric.y + pad + metric.z + pad + metric.z;

	// 상면도
	vps[1].h = _h = (float)height * metric.y / rh;
	vps[1].w = _w = _h * metric.x / metric.y;
	vps[1].x = 0;
	vps[1].y = 0;
	cams[1].setupOrtho(metric.x, metric.y);

	// 측면도
	vps[2].h = _h = (float)height * metric.z / rh;
	vps[2].w = _w = _h * metric.x / metric.z;
	vps[2].x = 0;
	vps[2].y = height * (metric.y + pad) / rh;
	cams[2].setupOrtho(metric.x, metric.z);

	// 정면도
	vps[3].h = _h = (float)height * metric.z / rh;
	vps[3].w = _w = _h * metric.y / metric.z;
	vps[3].x = 0;
	vps[3].y = height * (metric.y + pad + metric.z + pad) / rh;
	cams[3].setupOrtho(metric.y, metric.z);

	vps[0].h = _h = height;
	vps[0].w = _w = width - vps[1].w;
	if(vps[0].w < 0) vps[0].w = 0;
	vps[0].x = vps[1].w;
	vps[0].y = 0;
	cams[0].setupPerspect(_w / _h, 1);
	cams[4].setupPerspect(_w / _h, 1);
	cams[5].setupPerspect(_w / _h, 1);
	cams[6].setupPerspect(_w / _h, 1);
	cams[7].setupPerspect(_w / _h, 1);

	context.winWidth = width;
	context.winHeight = height;

	glutPostRedisplay();
}

void timer_scene(int timestamp_scene) {
	context.timestamp = timestamp_scene;
	tiger_data.cur_frame = timestamp_scene % N_TIGER_FRAMES;
	tiger_data.rotation_angle = (timestamp_scene % 360)*TO_RADIAN;
	glutPostRedisplay();
	glutTimerFunc(100, timer_scene, (timestamp_scene + 1) % INT_MAX);
}

void register_callbacks(void) {
	context.isLButtonDown = false;
	context.isRButtonDown = false;
	context.isCtrlDown = false;
	context.prevX = 0;
	context.prevY = 0;
	context.zoomFactor = 1;
	context.roamingTiger = false;

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouseCb);
	glutMotionFunc(motionCb);
	glutReshapeFunc(reshape);
	glutTimerFunc(100, timer_scene, 0);
	glutCloseFunc(cleanup_OpenGL_stuffs);
}

void prepare_shader_program(void) {
	ShaderInfo shader_info[3] = {
		{ GL_VERTEX_SHADER, "Shaders/simple.vert" },
		{ GL_FRAGMENT_SHADER, "Shaders/simple.frag" },
		{ GL_NONE, NULL }
	};
	h_ShaderProgram = LoadShaders(shader_info);
	glUseProgram(h_ShaderProgram);

	loc_ModelViewProjectionMatrix = glGetUniformLocation(h_ShaderProgram, "u_ModelViewProjectionMatrix");
	loc_primitive_color = glGetUniformLocation(h_ShaderProgram, "u_primitive_color");
}

void preparePath(void) {
	FILE *fp = fopen("Data/path_data.txt", "r");
	if(fp == NULL) {
		printf("-- Path file open error");
		return;
	}

	auto pathData = readPath(fp);
	path = Path(pathData);

	glGenBuffers(1, &pathVBO);
	glBindBuffer(GL_ARRAY_BUFFER, pathVBO);
	glBufferData(GL_ARRAY_BUFFER, path.path.size() * sizeof(glm::vec2), path.path.data(), GL_STATIC_DRAW);

	glGenVertexArrays(1, &pathVAO);
	glBindVertexArray(pathVAO);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, static_cast<GLvoid *>(0));
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void initialize_OpenGL(void) {
	glEnable(GL_DEPTH_TEST); // Default state
	glEnable(GL_SCISSOR_TEST);
	 
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glClearColor(1, 1, 1, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	::cameras[0] = Camera(
		glm::vec3(600.0f, 600.0f, 200.0f),
		glm::vec3(125.0f, 80.0f, 25.0f),
		glm::vec3(0.0f, 0.0f, 1.0f)
	);

	glm::vec3 metric(240, 170, 50);

	// 상면도 카메라
	::cameras[1] = Camera(
		glm::vec3(metric.x / 2, metric.y / 2, 1000),
		glm::vec3(metric.x / 2, metric.y / 2, 0),
		glm::vec3(0, 1, 0)
	);

	// 측면도 카메라
	::cameras[2] = Camera(
		glm::vec3(metric.x / 2, -1000, metric.z / 2),
		glm::vec3(metric.x / 2, metric.y, metric.z / 2),
		glm::vec3(0, 0, 1)
	);

	// 정면도 카메라
	::cameras[3] = Camera(
		glm::vec3(-1000, metric.y / 2, metric.z / 2),
		glm::vec3(metric.x, metric.y / 2, metric.z / 2),
		glm::vec3(0, 0, 1)
	);


	// 정적카메라 1
	::cameras[4] = Camera(
		glm::vec3(-100, -100, 100),
		glm::vec3(300, 300, 0),
		glm::vec3(0, 0, 1)
	);
	::cameras[4].farDist = 350;
	::cameras[4].zoom(0.3);

	// 정적카메라 2
	::cameras[5] = Camera(
		glm::vec3(120, -200, 200),
		glm::vec3(120, 120, 0),
		glm::vec3(0, 0, 1)
	);
	::cameras[5].farDist = 350;
	::cameras[5].zoom(0.3);

	// 정적카메라 3
	::cameras[6] = Camera(
		glm::vec3(224, 95, 25),
		glm::vec3(10, 95, 0),
		glm:: vec3(0, -1, 10)
	);
	::cameras[6].farDist = 200;
	::cameras[6].zoom(0.2);

	// 동적카메라 (어슬렁호랑이 카메라)
	::cameras[7] = Camera(glm::vec3(0, 0, 12), glm::vec3(10, 0, 12), glm::vec3(0, 0, 1));
	::cameras[7].nearDist = 1;
	::cameras[7].farDist = 135;
	::cameras[7].zoom(0.2);

	// ::cameras[8] = Camera(); 변환 후 어슬렁호랑이 카메라

	preparePath();
}

void prepare_scene(void) {
	ObjectDef2::defineObjects();
	define_axes();
	define_static_objects();
	define_animated_tiger();

	defineViewVolumeObject(volVAO, volVBO);
}

void initialize_renderer(void) {
	register_callbacks();
	prepare_shader_program();
	initialize_OpenGL();
	prepare_scene();
}

void initialize_glew(void) {
	GLenum error;

	glewExperimental = GL_TRUE;

	error = glewInit();
	if (error != GLEW_OK) {
		fprintf(stderr, "Error: %s\n", glewGetErrorString(error));
		exit(-1);
	}
	fprintf(stdout, "*********************************************************\n");
	fprintf(stdout, " - GLEW version supported: %s\n", glewGetString(GLEW_VERSION));
	fprintf(stdout, " - OpenGL renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(stdout, " - OpenGL version supported: %s\n", glGetString(GL_VERSION));
	fprintf(stdout, "*********************************************************\n\n");
}

void print_message(const char * m) {
	fprintf(stdout, "%s\n\n", m);
}

void greetings(char *program_name, char messages[][256], int n_message_lines) {
	fprintf(stdout, "**************************************************************\n\n");
	fprintf(stdout, "  PROGRAM NAME: %s\n\n", program_name);
	fprintf(stdout, "    This program was coded for CSE4170 students\n");
	fprintf(stdout, "      of Dept. of Comp. Sci. & Eng., Sogang University.\n\n");

	for (int i = 0; i < n_message_lines; i++)
		fprintf(stdout, "%s\n", messages[i]);
	fprintf(stdout, "\n**************************************************************\n\n");

	initialize_glew();
}

#define N_MESSAGE_LINES 1
void main(int argc, char *argv[]) { 
	char program_name[256] = "Sogang CSE4170 Our_House_GLSL_V_0.5";
	char messages[N_MESSAGE_LINES][256] = { "    - Keys used: 'c', 'f', 'd', 'ESC'" };

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);
	glutInitWindowSize(1440, 720);
	glutInitContextVersion(4, 0);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutCreateWindow(program_name);

	greetings(program_name, messages, N_MESSAGE_LINES);
	initialize_renderer();

	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
	glutMainLoop();
}
