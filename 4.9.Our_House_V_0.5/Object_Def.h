#pragma once

#include <stdio.h>
#include <string.h>
#include <vector>
#include <algorithm>
#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>

extern int loc_ModelViewProjectionMatrix;
extern int loc_primitive_color;
extern glm::mat4 ModelViewMatrix, ModelViewProjectionMatrix, ViewMatrix, ProjectionMatrix;

namespace ObjectDef2 {
	GLfloat const R_ANGLE = glm::half_pi<GLfloat>();
	glm::vec3 const BASIS_X = glm::vec3(1, 0, 0);
	glm::vec3 const BASIS_Y = glm::vec3(0, 1, 0);
	glm::vec3 const BASIS_Z = glm::vec3(0, 0, 1);

	enum struct ObjectId {
		BUILDING, TABLE, LIGHT, TEAPOT, NEW_CHAIR, FRAME, NEW_PICTURE, COW,
		BIKE, BUS, GODZILLA, IRONMAN, TANK, CAR_BODY, CAR_WHEEL, CAR_NUT
	};

	struct Material {
		static Material const DEFAULT_MATERIAL;

		glm::vec4 emission, ambient, diffuse, specular;
		GLfloat exponent;
	};

	Material const Material::DEFAULT_MATERIAL{
		glm::vec4(0.0f, 0.0f, 0.0f, 1.0f),
		glm::vec4(0.329412f, 0.223529f, 0.027451f, 1.0f),
		glm::vec4(0.780392f, 0.568627f, 0.113725f, 1.0f),
		glm::vec4(0.992157f, 0.941176f, 0.807843f, 1.0f),
		0.21794872f*0.6f
	};

	struct StaticObject {
		static int constexpr N_MAX_GEOM_COPIES = 5;
		static int constexpr N_MAX_STATIC_OBJECTS = 15;

		static StaticObject STATIC_OBJECTS[N_MAX_STATIC_OBJECTS];

		char filename[512];

		GLenum frontFaceMode; // clockwise or counter-clockwise
		int nTriangles;

		int nFields; // 3 floats for vertex, 3 floats for normal, and 2 floats for texcoord
		GLfloat *vertices = nullptr; // pointer to vertex array data
		glm::vec3 vMin, vMax;

		GLuint VBO = 0, VAO = 0; // Handles to vertex buffer object and vertex array object

		int nGeomInstances;

		glm::mat4 baseTransform;
		std::vector<glm::mat4> ModelMatrices;
		std::vector<Material> materials;

		void define(char const *_filename, int _nFields = 8, GLenum _frontFaceMode = GL_CCW) {
			strcpy(filename, _filename);
			nFields = _nFields;
			frontFaceMode = _frontFaceMode;

			auto nBytesPerVertex = sizeof(float) * _nFields;
			auto nBytesPerTriangle = 3 * nBytesPerVertex;

			readGeometry(nBytesPerTriangle);
			computeAABB();
			setupOpenGLObject(nBytesPerTriangle, nBytesPerVertex);

			auto center = (vMax + vMin) / 2.f;
			baseTransform = glm::translate(glm::mat4(), -center);
			vMax -= center;
			vMin -= center;
			free(vertices);
			vertices = nullptr;
		}

		void defineFromText(char const *_filename, int _nFields = 8, GLenum _frontFaceMode = GL_CCW) {
			strcpy(filename, _filename);
			nFields = _nFields;
			frontFaceMode = _frontFaceMode;

			auto nBytesPerVertex = sizeof(float) * _nFields;
			auto nBytesPerTriangle = 3 * nBytesPerVertex;

			readGeometryText(_nFields);
			computeAABB();
			setupOpenGLObject(nBytesPerTriangle, nBytesPerVertex);

			auto center = (vMax + vMin) / 2.f;
			baseTransform = glm::translate(glm::mat4(), -center);
			vMax -= center;
			vMin -= center;
			free(vertices);
			vertices = nullptr;
		}

		void updateBaseTransform(glm::mat4 Matrix) {
			baseTransform = Matrix * baseTransform;
			vMax = glm::vec3(Matrix * glm::vec4(vMax, 1));
			vMin = glm::vec3(Matrix * glm::vec4(vMin, 1));
			glm::vec3 s, t;
			s = glm::max(vMin, vMax);
			t = glm::min(vMin, vMax);

			vMax = s;
			vMin = t;
		}

		void addModel(glm::mat4 Matrix, Material material = Material::DEFAULT_MATERIAL) {
			Matrix = glm::translate(Matrix, glm::vec3(0, 0, (vMax.z - vMin.z) / 2));
			ModelMatrices.push_back(Matrix * baseTransform);
			materials.push_back(material);
			nGeomInstances = ModelMatrices.size();
		}

		void draw(int modelId) {
			glFrontFace(frontFaceMode);

			ModelViewMatrix = ViewMatrix * ModelMatrices[modelId];
			ModelViewProjectionMatrix = ProjectionMatrix * ModelViewMatrix;
			glUniformMatrix4fv(loc_ModelViewProjectionMatrix, 1, GL_FALSE, &ModelViewProjectionMatrix[0][0]);

			auto const &diffuse = materials[modelId].diffuse;
			glUniform3f(loc_primitive_color, diffuse.r, diffuse.g, diffuse.b);

			glBindVertexArray(VAO);
			glDrawArrays(GL_TRIANGLES, 0, 3 * nTriangles);
			glBindVertexArray(0);
		}

		glm::mat4 scaleToCube(float cubeLen) {
			auto v = vMax - vMin;
			auto scaleV = glm::vec3(cubeLen / std::max({ v.x, v.y, v.z }));

			return glm::scale(glm::mat4(1.f), scaleV);
		}

		float getXOffset() {
			return vMax.x - vMin.x;
		}

		float getYOffset() {
			return vMax.y - vMin.y;
		}

		float getZOffset() {
			return vMax.z - vMin.z;
		}

		static StaticObject &getStaticObject(ObjectId id) {
			return STATIC_OBJECTS[static_cast<int>(id)];
		}

		~StaticObject() {
			if(VAO) {
				glDeleteVertexArrays(1, &VAO);
			}
			if(VBO) {
				glDeleteBuffers(1, &VBO);
			}
		}

	private:
		void readGeometry(int nBytesPerPrimitive) {
			// fprintf(stdout, "Reading geometry from the geometry file %s...\n", filename);
			auto fp = fopen(filename, "rb");
			if(fp == NULL) {
				fprintf(stderr, "Error: cannot open the object file %s ...\n", filename);
				exit(EXIT_FAILURE);
			}
			fread(&nTriangles, sizeof(int), 1, fp);
			vertices = (float *)malloc(nTriangles*nBytesPerPrimitive);
			if(vertices == NULL) {
				fprintf(stderr, "Error: cannot allocate memory for the geometry file %s ...\n", filename);
				exit(EXIT_FAILURE);
			}
			fread(vertices, nBytesPerPrimitive, nTriangles, fp); // assume the data file has no faults.
			// fprintf(stdout, "Read %d primitives successfully.\n\n", n_triangles);
			fclose(fp);
		}

		void readGeometryText(int nFields) {
			// fprintf(stdout, "Reading geometry from the geometry file %s...\n", filename);
			auto fp = fopen(filename, "r");
			if(fp == NULL) {
				fprintf(stderr, "Error: cannot open the object file %s ...\n", filename);
				exit(EXIT_FAILURE);
			}

			fscanf(fp, "%d", &nTriangles);
			vertices = (float *)malloc(nTriangles*3 * nFields * sizeof(float));

			if(vertices == NULL) {
				fprintf(stderr, "Error: cannot allocate memory for the geometry file %s ...\n", filename);
				exit(EXIT_FAILURE);
			}

			auto ptr = vertices;
			for(int i = 0; i < 3 * nTriangles * nFields; i++)
				fscanf(fp, "%f", ptr++);

			// fprintf(stdout, "Read %d primitives successfully.\n\n", n_triangles);
			fclose(fp);
		}

		void computeAABB() {
			GLfloat xmin, xmax, ymin, ymax, zmin, zmax;
			xmin = ymin = zmin = INFINITY;
			xmax = ymax = zmax = -INFINITY;

			for(int i = 0; i < nTriangles; ++i) {
				for(int j = 0; j < 3; ++j) {
					float x, y, z;
					auto vertice = vertices + (i * 3 + j) * nFields;
					x = vertice[0];
					y = vertice[1];
					z = vertice[2];
					if(xmin > x) xmin = x;
					if(ymin > y) ymin = y;
					if(zmin > z) zmin = z;
					if(xmax < x) xmax = x;
					if(ymax < y) ymax = y;
					if(zmax < z) zmax = z;
				}
			}

			vMin.x = xmin;
			vMin.y = ymin;
			vMin.z = zmin;
			vMax.x = xmax;
			vMax.y = ymax;
			vMax.z = zmax;
		}

		void setupOpenGLObject(int nBytesPerTriangle, int nBytesPerVertex) {
			glGenBuffers(1, &VBO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, nTriangles * nBytesPerTriangle, vertices, GL_STATIC_DRAW);

			glGenVertexArrays(1, &VAO);
			glBindVertexArray(VAO);

			GLuint constexpr ATTR_VERTEX = 0;
			glVertexAttribPointer(ATTR_VERTEX, 3, GL_FLOAT, GL_FALSE, nBytesPerVertex, static_cast<GLvoid *>(0));
			glEnableVertexAttribArray(ATTR_VERTEX);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		}
	};

	StaticObject StaticObject::STATIC_OBJECTS[N_MAX_STATIC_OBJECTS];

	void defineBike() {
		auto &object = StaticObject::getStaticObject(ObjectId::BIKE);
		object.define("Data/static_object/Bike.geom");
		glm::mat4 Mat;
		Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(1, 0, 0));
		Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(0, 1, 0));
		object.updateBaseTransform(Mat);

		object.addModel(object.scaleToCube(100));

		Mat = glm::translate(glm::mat4(), glm::vec3((185 + 225) / 2, 155, 25));
		Mat = glm::rotate(Mat, R_ANGLE, BASIS_X);
		Mat = glm::scale(Mat, glm::vec3(40. / object.getXOffset()));
		object.addModel(Mat);

	}

	void defineBus() {
		auto &object = StaticObject::getStaticObject(ObjectId::BUS);
		object.define("Data/static_object/Bus.geom");
		glm::mat4 Mat;
		Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(1, 0, 0));
		Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(0, 1, 0));
		object.updateBaseTransform(Mat);

		object.addModel(object.scaleToCube(100));

		Mat = glm::translate(glm::mat4(), glm::vec3((70 + 145) / 2, (125 + 140) / 2, 0));
		Mat = glm::scale(Mat, glm::vec3(15. / object.getYOffset()));
		object.addModel(Mat);
	}

	void defineGodzilla() {
		auto &object = StaticObject::getStaticObject(ObjectId::GODZILLA);
		object.define("Data/static_object/Godzilla.geom");
		glm::mat4 Mat;
		Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(1, 0, 0));
		Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(0, 1, 0));
		object.updateBaseTransform(Mat);

		object.addModel(object.scaleToCube(100));

		Mat = glm::translate(glm::mat4(), glm::vec3((125 + 165) / 2, (25 + 50) / 2, 0));
		Mat = glm::rotate(Mat, R_ANGLE * 2, BASIS_Z);
		Mat = glm::scale(Mat, glm::vec3(40. / object.getXOffset()));
		object.addModel(Mat);
	}

	void defineIronMan() {
		auto &object = StaticObject::getStaticObject(ObjectId::IRONMAN);
		object.define("Data/static_object/IronMan.geom");
		glm::mat4 Mat;
		Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(1, 0, 0));
		Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(0, 1, 0));
		object.updateBaseTransform(Mat);

		object.addModel(object.scaleToCube(100));

		auto scale = 40. / object.getYOffset();
		Mat = glm::translate(glm::mat4(), glm::vec3((15 + 55) / 2, 60 - object.getXOffset()*scale/2, 0));
		Mat = glm::rotate(Mat, -R_ANGLE, BASIS_Z);
		Mat = glm::scale(Mat, glm::vec3(scale));
		object.addModel(Mat);
	}

	void defineTank() {
		auto &object = StaticObject::getStaticObject(ObjectId::TANK);
		object.define("Data/static_object/Tank.geom");
		glm::mat4 Mat;
		Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(0, 0, 1));
		object.updateBaseTransform(Mat);

		object.addModel(object.scaleToCube(100));

		Mat = glm::translate(glm::mat4(), glm::vec3((30 + 65) / 2, 110, 25));
		Mat = glm::rotate(Mat, R_ANGLE, BASIS_X);
		Mat = glm::rotate(Mat, R_ANGLE, BASIS_Z);
		Mat = glm::scale(Mat, glm::vec3(50. / object.getXOffset()));
		object.addModel(Mat);
	}

	void defineCarBody() {
		auto &object = StaticObject::getStaticObject(ObjectId::CAR_BODY);
		object.defineFromText("Data/car_body_triangles_v.txt", 3);
		glm::mat4 Mat;
		// Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(0, 0, 1));
		object.updateBaseTransform(Mat);
		object.addModel(object.scaleToCube(100));
	}

	void defineCarWheel() {
		auto &object = StaticObject::getStaticObject(ObjectId::CAR_WHEEL);
		object.defineFromText("Data/car_wheel_triangles_v.txt", 3);
		glm::mat4 Mat;
		//Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(0, 0, 1));
		object.updateBaseTransform(Mat);
		object.addModel(object.scaleToCube(100));
	}
	
	void defineCarNut() {
		auto &object = StaticObject::getStaticObject(ObjectId::CAR_NUT);
		object.defineFromText("Data/car_nut_triangles_v.txt", 3);
		glm::mat4 Mat;
		//Mat = glm::rotate(Mat, R_ANGLE, glm::vec3(0, 0, 1));
		object.updateBaseTransform(Mat);
		object.addModel(object.scaleToCube(100));
	}

	void defineObjects() {
		defineBike();
		defineBus();
		defineGodzilla();
		defineIronMan();
		defineTank();

		defineCarBody();
		defineCarNut();
		return; 
		defineCarWheel();
	}
}