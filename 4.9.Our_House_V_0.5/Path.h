#pragma once

#include <vector>
#include <tuple>
#include <glm/geometric.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/trigonometric.hpp>

std::vector<glm::vec2> readPath(FILE *file) {
	std::vector<glm::vec2> path;

	while (!feof(file)) {
		float x, y;
		if (fscanf(file, "%f %f", &x, &y) != 2) continue;
		path.push_back({ x, y });
	}

	return path;
}

struct Path {
	std::vector<glm::vec2> path;
	std::vector<float> distSum;
	float curveThreshold = 10.;

	Path() {};

	Path(std::vector<glm::vec2> _path) : path(_path) {
		distSum.resize(_path.size());
		distSum[0] = 0.;

		for (int i = 1; i < distSum.size(); ++i) {
			distSum[i] = distSum[i-1] + glm::distance(path[i], path[i - 1]);
		}
	}

	float getTotalDist() {
		return distSum[distSum.size() - 1];
	}

	std::tuple<glm::vec2, glm::vec2> calcPosition(float curDist) {
		int i = 1;
		while (i < distSum.size() && distSum[i] < curDist) ++i;
		if (i >= distSum.size()) {
			i = path.size() - 1;
			return std::make_tuple(path[i], path[i] - path[i - 1]);
		}

		auto t = (curDist - distSum[i - 1]) / (distSum[i] - distSum[i - 1]);

		auto P = (1 - t)*path[i - 1] + t * path[i];
		auto v = path[i] - path[i - 1];
		auto L = glm::length(v);
		auto h = curveThreshold;

		// smooth curve
		if(L*t < h) {
			// near start of edge
			if(i == 1) {
				return std::make_tuple(P, v);
			}

			P = (1 - h / L) * path[i - 1] + (h / L) * path[i];

			auto v1 = glm::vec3(path[i - 1] - path[i - 2], 0);
			auto v2 = glm::vec3(v, 0);
			auto axis = glm::cross(v1, v2);
			auto n = glm::normalize(glm::cross(axis, v2));

			float t_star = L * t / h;
			float theta = glm::acos(-glm::dot(glm::normalize(v1), glm::normalize(v2)));
			if(theta < glm::quarter_pi<float>()) {
				return std::make_tuple(P, v);
			}
			float alpha = (glm::pi<float>() - theta) / 2 * (1 - t_star);

			float radius = h * glm::tan(theta / 2);
			auto center = glm::vec3(P, 0) + radius * n;

			glm::mat4 Mat;
			Mat = glm::rotate(Mat, -alpha, axis);

			auto arcVtor = Mat * glm::vec4(-radius * n, 0);

			return std::make_tuple(
				glm::vec2(glm::vec4(center, 1) + arcVtor),
				glm::vec2(glm::cross(axis, glm::vec3(arcVtor)))
			);
		}
		else if(L*t > L - h) {
			// near end of edge
			if(i == path.size() - 1) {
				return std::make_tuple(P, v);
			}

			P = (1 - h / L) * path[i] + (h / L) * path[i-1];

			auto v1 = glm::vec3(v, 0);
			auto v2 = glm::vec3(path[i+1] - path[i], 0);
			auto axis = glm::cross(v1, v2);
			auto n = glm::normalize(glm::cross(axis, v1));

			float t_star = (L * t - (L - h)) / h;
			float theta = glm::acos(-glm::dot(glm::normalize(v1), glm::normalize(v2)));
			if(theta < glm::quarter_pi<float>()) {
				return std::make_tuple(P, v);
			}
			float alpha = (glm::pi<float>() - theta) / 2 * t_star;

			float radius = h * glm::tan(theta / 2);
			auto center = glm::vec3(P, 0) + radius * n;

			glm::mat4 Mat;
			Mat = glm::rotate(Mat, alpha, axis);

			auto arcVtor = Mat * glm::vec4(-radius*n, 0);

			return std::make_tuple(
				glm::vec2(glm::vec4(center, 1) + arcVtor),
				glm::vec2(glm::cross(axis, glm::vec3(arcVtor)))
			);
		}
		else {
			return std::make_tuple(P, v);
		}
	}
};
